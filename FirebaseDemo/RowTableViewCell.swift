//
//  RowTableViewCell.swift
//  FirebaseDemo
//
//  Created by Ninja on 06/09/19.
//  Copyright © 2019 Ninja. All rights reserved.
//

import UIKit

class RowTableViewCell: UITableViewCell {

    @IBOutlet weak var studImageView: UIImageView!
    @IBOutlet weak var studLastName: UILabel!
    @IBOutlet weak var studFirstName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        studImageView.layer.cornerRadius = studImageView.frame.width/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
