//
//  StudentModel.swift
//  FirebaseDemo
//
//  Created by Ninja on 06/09/19.
//  Copyright © 2019 Ninja. All rights reserved.
//

import Foundation

class StudentModel {
    
    var id: String?
    var firstName: String?
    var lastName: String?
    var motherName: String?
    var fatherName: String?
    var gender: String?
    var profileImage: String?
    
    
    init(id: String?, firstName: String?, lastName: String?,motherName: String?,fatherName: String?,gender: String?,profileImage: String?){
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.motherName = motherName
        self.fatherName = fatherName
        self.gender = gender
        self.profileImage = profileImage
    }
}
