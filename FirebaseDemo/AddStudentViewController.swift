//
//  AddStudentViewController.swift
//  FirebaseDemo
//
//  Created by Ninja on 06/09/19.
//  Copyright © 2019 Ninja. All rights reserved.
//

import UIKit
import FirebaseDatabase

class AddStudentViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var txtMotherName: UITextField!
    @IBOutlet weak var txtFatherName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var maleRadioButton: UIImageView!
    @IBOutlet weak var femaleRadioButton: UIImageView!
    
    weak var delegate:updateTableDelegate?
  
    let imagePicker = UIImagePickerController()
    var ref: DatabaseReference!
    var gender: NSString = ""
    var profileImage: NSString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        profileImageView.layer.cornerRadius = profileImageView.frame.width/2

    }

    @IBAction func uploadImageTapped(_ sender: Any) {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            
            profileImageView.image = pickedImage
            
            let compressedImage  = pickedImage.resizeWithWidth(width: 500)!
            let imageData:NSData = compressedImage.pngData()! as NSData
            profileImage = imageData.base64EncodedString(options: .lineLength64Characters) as NSString
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnMaleTapped(_ sender: Any) {
        gender = "M"
        maleRadioButton.image = UIImage(named: "selected")
        femaleRadioButton.image = UIImage(named: "notSelected")
    }
    
    @IBAction func btnFemaleTapped(_ sender: Any) {
         gender = "F"
        maleRadioButton.image = UIImage(named: "notSelected")
        femaleRadioButton.image = UIImage(named: "selected")
    }
    
    @IBAction func btnSaveTapped(_ sender: Any) {
        
        if profileImage.length == 0 {
            displayAlert(message: "Select your photo")
            return
        }
        
        if self.txtFirstName.text == "" {
            displayAlert(message: "Enter first name")
            return
        }
        
        if self.txtLastName.text == "" {
            displayAlert(message: "Enter last name")
            return
        }
        
        if self.txtMotherName.text == "" {
            displayAlert(message: "Enter mother name")
            return
        }
        
        if self.txtFatherName.text == "" {
            displayAlert(message: "Enter father name")
            return
        }
        
        if gender.length == 0{
            displayAlert(message: "Select gender")
            return
        }
        
        ref = Database.database().reference().child("Users")
        guard let userId = UserDefaults.standard.string(forKey: "userId") else { return  }

        let key = ref.childByAutoId().key
        let student = ["studentFirstName": txtFirstName.text ?? "","studentLastName": txtLastName.text ?? "","studentMotherName": txtMotherName.text ?? "","studentFatherName": txtFatherName.text ?? "" ,"studentId":key ?? "","studentGender":gender ,"studentProfileImage":profileImage ] as [String : Any]
        ref.child(userId).child(key ?? "100").setValue(student)
        
        self.delegate?.reloadData()
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func displayAlert(message : NSString){
        let alertController = UIAlertController(title: "", message: message as String, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        
        present(alertController, animated: true, completion: nil)
    }
}

extension UIImage {
    func resizeWithPercent(percentage: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    func resizeWithWidth(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}
