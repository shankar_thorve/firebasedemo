//
//  ViewController.swift
//  FirebaseDemo
//
//  Created by Ninja on 05/09/19.
//  Copyright © 2019 Ninja. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class ViewController: UIViewController {

    @IBOutlet weak var txtEmailId: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Login"
    }

    @IBAction func btnLoginTapped(_ sender: Any) {
        if self.txtEmailId.text == "" || self.txtPassword.text == "" {
            
            //Alert to tell the user that there was an error because they didn't fill anything in the textfields because they didn't fill anything in
            
            let alertController = UIAlertController(title: "Error", message: "Please enter an email and password.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            
            Auth.auth().signIn(withEmail: self.txtEmailId.text!, password: self.txtPassword.text!) { (user, error) in
                
                if error == nil {
                    
                    //Print into the console if successfully logged in
                    print("You have successfully logged in")
                   
                    UserDefaults.standard.set(true, forKey: "isLogin") //Bool
                    UserDefaults.standard.set(user?.user.uid, forKey: "userId")
                    //Go to the HomeViewController if the login is sucessful
                    let vc = HomeViewViewController(nibName: "HomeViewViewController", bundle: nil)
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                } else {
                    
                    //Tells the user that there is an error and then gets firebase to tell them the error
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
}
    
    
    @IBAction func btnSignUpTapped(_ sender: Any) {
        let vc = SignUpViewController(nibName: "SignUpViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

