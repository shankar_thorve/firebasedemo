//
//  SignUpViewController.swift
//  FirebaseDemo
//
//  Created by Ninja on 05/09/19.
//  Copyright © 2019 Ninja. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class SignUpViewController: UIViewController {

    @IBOutlet weak var txtEmailId: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Sign Up"
        // Do any additional setup after loading the view.
    }

    @IBAction func btnSignUpTapped(_ sender: Any) {
        if txtEmailId.text == "" {
            let alertController = UIAlertController(title: "Error", message: "Please enter your email and password", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
            
        } else {
            Auth.auth().createUser(withEmail: txtEmailId.text!, password: txtPassword.text!) { (user, error) in
                if error == nil {
                    print("You have successfully signed up")
                    UserDefaults.standard.set(true, forKey: "isLogin") //Bool
                    UserDefaults.standard.set(user?.user.uid, forKey: "userId")
                    let vc = HomeViewViewController(nibName: "HomeViewViewController", bundle: nil)
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
}
