//
//  HomeViewViewController.swift
//  FirebaseDemo
//
//  Created by Ninja on 05/09/19.
//  Copyright © 2019 Ninja. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

protocol updateTableDelegate:class {
    func reloadData()
}

class HomeViewViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,updateTableDelegate {
   
    
    @IBOutlet weak var listTableView: UITableView!
    
    var studentList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "HomeView"
        listTableView.register(UINib(nibName: "RowTableViewCell", bundle: nil), forCellReuseIdentifier: "rowTableViewCell")
        fetchDataFromDataBase();
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        self.navigationItem.rightBarButtonItem  = add
    }

    @objc func addTapped(){
        let vc = AddStudentViewController(nibName: "AddStudentViewController", bundle: nil)
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.studentList.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "rowTableViewCell", for: indexPath) as! RowTableViewCell
        let studentModel = studentList.object(at: indexPath.row) as! StudentModel
        cell.studFirstName.text = studentModel.firstName
        cell.studLastName.text = studentModel.lastName
        if (studentModel.profileImage != nil){
            cell.studImageView?.image = base64Convert(base64String: studentModel.profileImage)
        }
        return cell
    }
    
    
    func fetchDataFromDataBase(){
        studentList.removeAllObjects()
        var ref: DatabaseReference!
        ref = Database.database().reference().child("Users")
        guard let userId = UserDefaults.standard.string(forKey: "userId") else { return  }
        
        ref.child(userId).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            for students in snapshot.children.allObjects as! [DataSnapshot] {
                let studentObject = students.value as? [String: AnyObject]
                let studentName  = studentObject?["studentFirstName"]
                let studentId  = studentObject?["studentId"]
                let studentLastName = studentObject?["studentLastName"]
                let studentProfileImage = studentObject?["studentProfileImage"]
                
                //creating student object with model and fetched values
                let studentModel = StudentModel(id: studentId as! String?, firstName: studentName as! String?, lastName: studentLastName as! String?, motherName: "", fatherName: "", gender: "", profileImage: studentProfileImage as! String?)
                
                //appending it to list
                self.studentList.add(studentModel)
            }
            self.listTableView.reloadData()

        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func reloadData() {
        fetchDataFromDataBase()
    }
    
    func base64Convert(base64String: String?) -> UIImage{
        if (base64String?.isEmpty)! {
            return UIImage()
        }else {
            let dataDecoded : Data = Data(base64Encoded: base64String!, options: .ignoreUnknownCharacters)!
            let decodedimage = UIImage(data: dataDecoded)
            return decodedimage!
        }
    }
}

